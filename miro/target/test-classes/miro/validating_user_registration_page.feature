Feature: validating user registration page
  Scenario: Verify that all fields and buttons are available on sign up page
    Given I am on signup page
    When I search for these elements and buttons by their ids"
    Then I will be able to find them present on the page

  Scenario: Validate empty required fields
    Given All resources on sign up page are loaded
    When I click on submit button without filling required fields
    Then I should see error messages
   | LabelIds | ErrorMessages |
   | nameError | Please enter your name. |
   | emailError | Please enter your email address. |
   | passwordError | Please enter your password. |
   | termsError | Please agree with the Terms to sign up. |

  Scenario: Submit correctly completed field with unchecked Terms and Conditions
    Given I have correctly completed the form
      | fields | content |
      | name | James Gardener |
      | email | james.gardener@gmail.com |
      | password | P1p3t@mir0 |
    When I submit the form without checking the Terms and Conditions checkbox
    Then I get this "Please agree with the Terms to sign up." as response

  Scenario: Submit correctly completed field with checked T and C
    Given I have correctly completed the form
      | fields | content |
      | name | James Gardener |
      | email | james.gardener@gmail.com |
      | password | P1p3t@mir0 |
    When I check the Terms and Conditions checkbox and submit the form
    Then I should be sent to the email confirmation page with titled "Check your email"

  Scenario: Submit correctly completed field with unchecked signup updates
    Given I have correctly completed the form
      | fields | content |
      | name | James Gardener |
      | email | james.gardene@gmail.com |
      | password | P1p3t@mir0 |
    When I check the Terms and Conditions checkbox, uncheck the signup updates checkbox and submit the form
    Then I should be sent to the email confirmation page with titled "Check your email"

  Scenario: Submit correctly completed field with unchecked signup updates
    Given I have correctly completed the form
      | fields | content |
      | name | James Gardener |
      | email | james.gardener@gmail.com |
      | password | P1p3t@mir0 |
    When I check the Terms and Conditions checkbox, check the signup updates checkbox and submit the form
    Then I should be sent to the email confirmation page with titled "Check your email"

  Scenario: Validate various password entries for character length and type of characters
    Given I am on sign up page
    When I search and find the password input field by id "password"
    Then I enter these passwords in the "password" fields and get the appropriate response
      | password | dataId | response |
      | ew_-k6   | data-text-default | Please use 8+ characters for secure password |
      | mysafepassword| data-text-soso | So-so password |
      | 12345678      | data-text-weak | Weak password |
      | ew_-k6i-p"P#Gay| data-text-good | Good password |


  Scenario Outline: Validating email addresses both correct and incorrect emails
    Given I am on sign up page
    When I complete the fields "James Baxter", "ew_-k6i-p@P#Gay" and "<email>"
    And I check the Terms and Conditions checkbox and submit the form
    Then I get the following "<responses>"
    Examples:
      | email                  | responses                                                                         |
      | @gmail.com             | The email you entered is incorrect.                                               |
      | jamesgardenergmail.com | The email you entered is incorrect.                                               |
      | jamesgardener@gmail    | This doesn’t look like an email address. Please check it for typos and try again. |
      | jamesgardener@gmailcom | This doesn’t look like an email address. Please check it for typos and try again. |
      | jamesbaxter@1.01.6.4   | This doesn’t look like an email address. Please check it for typos and try again. |
      | james.baxter@gmail.com | Sorry, this email is already registered                                           |
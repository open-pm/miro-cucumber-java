package miro;

import io.cucumber.core.backend.StepDefinition;
import io.cucumber.core.gherkin.Step;
import io.cucumber.core.gherkin.StepType;
import io.cucumber.java.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Hooks {

    private static WebDriver webdriver;
    String miroSignUpPage = "https://miro.com/signup";
    static String screenShotPath = "screen_shots/";

    @Before
    public void beforeCucumber(Scenario scenario) {
        scenario.log("---------- "+ scenario.getId()+ " Testing "+scenario.getName()+ " ----------");
        webdriver = new FirefoxDriver();
        webdriver.get(miroSignUpPage);
        webdriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
    }

    @After
    public void afterCucumber(Scenario scenario) {
        try{
        String message = scenario.isFailed()? "Scenario failed" : "Scenario passed";
        getScreenShot(getFilePath()+""+scenario.getId()+""+generateNameForScreenShot()+".png");
        scenario.log(message);
        scenario.getStatus().toString();
        scenario.log("---------- Ending scenario test ----------");
        getWebdriver().close();
        }catch(NoSuchSessionException noSuchSessionException){
            scenario.log("Session not found Exception thrown here");
        }
    }

    @BeforeStep
    public void beforeEachStep(Scenario scenario){
        scenario.log("Stepping into step");
    }

    @AfterStep
    public void afterEachStep(Scenario scenario){
        if(scenario.isFailed())
            getScreenShot(getFilePath()+"_after_"+scenario.getId()+""+generateNameForScreenShot()+".png");
        scenario.log("Step completed");
    }

    public static WebDriver getWebdriver() {
        return webdriver;
    }

    public static String getFilePath(){
        return screenShotPath;
    }

    private static void takeSnapShot(String filename) throws Exception{
        TakesScreenshot screenShot =((TakesScreenshot)getWebdriver());
        File SrceenFile=screenShot.getScreenshotAs(OutputType.FILE);
        File DestinationFile=new File(filename);
        FileUtils.copyFile(SrceenFile, DestinationFile);
    }

    public static void getScreenShot(String filename){
        try{
            takeSnapShot(filename);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Could not take screenshot");
        }
    }

    public static String generateNameForScreenShot() {
        byte[] array = new byte[3]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedName = new String(array, Charset.forName("UTF-8"));
        return generatedName;
    }
}
package miro;

import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Map;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.datatable.DataTable;
import junit.framework.AssertionFailedError;
import org.junit.ComparisonFailure;
import org.openqa.selenium.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.junit.Assert.assertEquals;

public class StepDefinitions {

    private static Logger log = LogManager.getLogger(StepDefinitions.class);
    Hooks hooks = new Hooks();

    private Boolean findElement = false;


    @Given("^I am on signup page$")
    public void i_am_on_signup_page() throws Exception {
        log.info(hooks.getWebdriver().getTitle());
    }

    @When("I search for these elements and buttons by their ids\"")
    public void i_search_for_these_and_buttons_by_their_ids() throws NoSuchElementException {
        String[] element_ids = {"name", "email", "password", "signup-subscribe", "signup-terms", "nameError", "emailError", "passwordError",
                "invite", "guest", "termsError", "cookie_enable_notice", "js-time-zone-id", "js-time-zone-offset", "password-hint", "signup-form-password"};
        for (String id : element_ids) {
            try {
                // search for elements by their id
                WebElement element = hooks.getWebdriver().findElement(By.id(id));
                log.info("Element found");
                log.info("Step passed");
            } catch (NoSuchElementException e) {
                log.warn("The element with " + id + " could be found on signup page");
                log.error("Test failed");
                break;
            }
        }
        log.info("Step passed");
    }

    @Then("^I will be able to find them present on the page$")
    public void i_will_be_able_to_find_them_present_on_the_field() throws Exception {
        log.info("All expected elements are present on sign up page. Test passed");
        hooks.getWebdriver().close();
    }

    @Given("All resources on sign up page are loaded")
    public void all_resources_on_sign_up_page_are_loaded() {
        log.info("Ready to start test");
    }

    @When("I click on submit button without filling required fields")
    public void i_click_on_submit_button_without_filling_required_fields() {
        try {
            //search for submit button using the data-autotest-id
            WebElement submitButton = hooks.getWebdriver().findElement(By.cssSelector("button[data-autotest-id='mr-form-signup-btn-start-1']"));
            log.info("Submit button found");
            submitButton.click();
            log.info("Submit button clicked");
            log.info("Step passed");
        } catch (Exception e) {
            log.warn("Submit button could not be found");
            log.error("Step failed");
        }
    }

    @Then("I should see error messages")
    public void i_should_see_error_messages(DataTable dataTable) {
        // Passing test data from datatable to a List of Maps- the map has the labelIds as key and the message as the value
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        String actualMessage = null;
        // Test using data from each
        for (Map<String, String> columns : rows) {
            try {
                //Using a different locator (cssSelector) to search for password error message label
                if (columns.get("LabelIds").equals("passwordError")) {
                    actualMessage = hooks.getWebdriver().findElement(By.cssSelector("div[data-autotest-id='please-enter-your-password-1']")).getText();
                    log.info("Label found and text extracted");
                } else {
                    //Using a different locator (id) to search for password error message label
                    actualMessage = hooks.getWebdriver().findElement(By.id(columns.get("LabelIds"))).getText();
                    log.info("Label found and text extracted");
                }
                assertEquals(columns.get("ErrorMessages"),actualMessage);
                log.info("Expected and actual messages match");

            } catch (NoSuchElementException e) {
                log.warn("The element with " + columns.get("LabelIds") + " could be found on signup page");
                log.error("Step Failed");
                break;
            }catch (ComparisonFailure failedError){
                log.warn("Expected error message is different from actual message error");
                log.warn("Expected: " + columns.get("ErrorMessages"));
                log.warn("Actual: " + actualMessage);
            }
        }
        log.info("Step has passed");
    }

    @Given("I have correctly completed the form")
    public void i_have_correctly_completed_the_form(DataTable dataTable) {
        // Passing test data from datatable to a List of Maps- the map has the field_id as key and the message as the value
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        try {

            WebElement nameField = hooks.getWebdriver().findElement(By.id(rows.get(0).get("fields")));
            log.info("Name input field found");
            WebElement emailField = hooks.getWebdriver().findElement(By.id(rows.get(1).get("fields")));
            log.info("Email input field found");
            WebElement passwordField = hooks.getWebdriver().findElement(By.id(rows.get(2).get("fields")));
            log.info("Password input field found");

            log.info("Enter "+rows.get(0).get("content") +" as the name");
            nameField.sendKeys(rows.get(0).get("content"));
            log.info("Enter "+rows.get(1).get("content") +" as the email");
            emailField.sendKeys(rows.get(1).get("content"));
            log.info("Enter "+rows.get(2).get("content") +" as the password");
            passwordField.sendKeys(rows.get(2).get("content"));
            log.info("Step passed");
        } catch (Exception o) {
            log.warn("Elements cannot be found");
            log.error("Step failed");
        }
    }

    @When("I submit the form without checking the Terms and Conditions checkbox")
    public void i_submit_the_form_without_checking_the_terms_and_conditions_checkbox() {
        try {
            //search for submit button
            WebElement submitButton = hooks.getWebdriver().findElement(By.cssSelector("button[data-autotest-id='mr-form-signup-btn-start-1']"));
            log.info("Submit button found");
            submitButton.click();
            log.info("Submit button clicked");
            log.info("Step passed");
        } catch (Exception e) {
            log.warn("Submit button could not be found");
            log.error("Step failed");
        }
    }

    @Then("I get this {string} as response")
    public void i_get_this_as_response(String actual) {
        WebElement labelText = null;
        try {
            //search for the label displaying the error message for unchecked T and C
            labelText = hooks.getWebdriver().findElement(By.id("termsError"));
            log.info("Error text label found");
            assertEquals(labelText.getText(),actual);
            log.info("Expected message matches actual message");
            log.info("Step failed");
        } catch (NoSuchElementException e) {
            log.warn("Element cannot be found, test failed");
            log.error("Step failed");
        }catch (ComparisonFailure failedError){
            log.error("Expected error message and actual messages do not match");
            log.info("Expected message: " + labelText.getText());
            log.info("Actual message: " + actual);
            log.error("Step failed");
        }
    }

    @Given("I am on sign up page")
    public void i_am_on_sign_up_page() {
        log.info("Successfully accessed the url");
    }

    @When("I search and find the password input field by id {string}")
    public void i_search_and_find_the_password_input_field_by_id(String password) {
        try {
            //search for the password field
            hooks.getWebdriver().findElement(By.id(password));
            log.info("Password field input field found");
            log.info("Step passed");
        } catch (Exception e) {
            log.warn("Input field could not be found");
            log.error("Step failed");
        }
    }

    @Then("I enter these passwords in the {string} fields and get the appropriate response")
    public void i_enter_these_passwords_in_the_fields_and_get_the_appropriate_response(String password, DataTable dataTable) {
        WebElement passwordStrengthLabel = null;
        // Passing test data from datatable to a List of Maps- the map has the field_id as key and the message as the value
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        for (int i = 0; i < rows.size(); i++) {
            try {
                WebElement passwordField = hooks.getWebdriver().findElement(By.id(password));
                rows.get(i).get("Password inout box found");
                log.info("Entering -- " + rows.get(i).get("password") + " -- in the password field");
                passwordField.sendKeys(rows.get(i).get("password"));
                passwordStrengthLabel = hooks.getWebdriver().findElement(By.cssSelector("div[" + rows.get(i).get("dataId") + "='" + rows.get(i).get("response") + "']"));
                log.info("Label displaying password error found");
                assertEquals(passwordStrengthLabel.getText(), rows.get(i).get("response"));
                log.info("Expected and actual error messages match");
            }catch (Exception e) {
                log.warn("Cannot find the password input field");
                log.error("Step failed");
            }catch (ComparisonFailure failedError){
                log.info("Expected and actual error messages do not match");
                log.info("Expected: " + rows.get(i).get("response"));
                log.info("Actual: " + passwordStrengthLabel.getText());
                log.error("Step failed");
            }
            hooks.getWebdriver().navigate().refresh();
        }
        log.info("Step passed");
    }

    @When("I check the Terms and Conditions checkbox and submit the form")
    public void i_check_the_terms_and_conditions_checkbox_and_submit_the_form() {
        try {
            WebElement checkbox = hooks.getWebdriver().findElement(By.className("mr-checkbox-1__icon"));
            log.info("T & C check box found");
            WebElement submitButton = hooks.getWebdriver().findElement(By.cssSelector("button[data-autotest-id='mr-form-signup-btn-start-1']"));
            log.info("Submit button found");
            checkbox.click();
            log.info("Terms and condition checked");
            submitButton.click();
            log.info("Submit button clicked");
            log.error("Step passed");
        } catch (Exception no) {
            log.error("Step failed");
        }

    }

    @Then("I should be sent to the email confirmation page with titled {string}")
    public void i_should_be_sent_to_the_email_confirmation_page_with_titled(String response) {
        try {
            // Searching for the confirmation message after successful registration
            WebElement confirmationLabel = hooks.getWebdriver().findElement(By.className("signup__title-form"));
            log.info("Confirmation message for successful registration found");
            assertEquals(confirmationLabel.getText(),response);
            log.info("Step passed");

        } catch (Exception e) {
            log.error("Step failed");
        } catch (ComparisonFailure failedError){
            log.warn("Step failed");
        }
    }

    @When("I check the Terms and Conditions checkbox, uncheck the signup updates checkbox and submit the form")
    public void i_check_the_terms_and_conditions_checkbox_uncheck_the_signup_updates_checkbox_and_submit_the_form() {
        try {
            List<WebElement> checkboxes = hooks.getWebdriver().findElements(By.className("mr-checkbox-1__icon"));
            log.info("Checkboxes for terms & conditions and news subscription found");
            WebElement submitButton = hooks.getWebdriver().findElement(By.cssSelector("button[data-autotest-id='mr-form-signup-btn-start-1']"));
            log.info("Submit button found");
            checkboxes.get(0).click();
            log.info("Subscribe for news checkbox clicked");
            submitButton.click();
            log.info("Submit button clicked");
            log.info("Step passed");
        } catch (Exception no) {
            log.error("Step failed");
        }
    }

    @When("I check the Terms and Conditions checkbox, check the signup updates checkbox and submit the form")
    public void i_check_the_terms_and_conditions_checkbox_check_the_signup_updates_checkbox_and_submit_the_form() {
        try {
            List<WebElement> checkboxes = hooks.getWebdriver().findElements(By.className("mr-checkbox-1__icon"));
            log.info("Checkboxes found");
            WebElement submitButton = hooks.getWebdriver().findElement(By.cssSelector("button[data-autotest-id='mr-form-signup-btn-start-1']"));
            log.info("Submit button found");
            checkboxes.get(0).click();
            log.info("Terms and conditions checkbox clicked");
            checkboxes.get(1).click();
            log.info("Subscribe for news checkbox clicked");
            submitButton.click();
            log.info("Submit button clicked");
            log.info("Step passed");
        } catch (Exception no) {
            log.fatal("The element cannot be found");
            log.error("Step failed");
        }
    }

    @When("I complete the fields {string}, {string} and {string}")
    public void i_complete_the_fields_and(String name, String password, String email) {
        try {
            WebElement nameInput = hooks.getWebdriver().findElement(By.id("name"));
            log.info("Name input element was found");
            WebElement emailInput = hooks.getWebdriver().findElement(By.id("email"));
            log.info("Email input element was found");
            WebElement passwordInput = hooks.getWebdriver().findElement(By.id("password"));
            log.info("Password input element was found");
            nameInput.sendKeys(name);
            log.info(name+" Name was entered correctly");
            emailInput.sendKeys(email);
            log.info(email+ " email entered correctly");
            passwordInput.sendKeys(password);
            log.info(password+ " Password entered correctly");
            log.error("Step passed");
        } catch (Exception e) {
            log.fatal("Name input element was found");
            log.error("Step failed");
        }
    }

    @Then("I get the following {string}")
    public void i_get_the_following(String response) {
        WebElement emailError = null;
        try {
            emailError = hooks.getWebdriver().findElement(By.id("emailError"));
            log.info("Email error label found");
            assertEquals(emailError.getText(),response);
                log.info("Error massages match");
                log.info("Step passed");


        } catch (NoSuchElementException e) {
            log.fatal("Some elements cannot be found");
            log.error("Step failed");
        } catch (ComparisonFailure assertionError){
            log.error("Error massages does not match");
            log.info("Expected: " + response);
            log.info("Actual: " + emailError.getText());
            log.error("Step failed");

        }
    }

}
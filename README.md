# miro-cucumber-java
***

## Name
Setup Documentaion

## Description
This project is an automated test for the miro signup page. It consist of Selenium Webdriver, Cucumber for BDD and Gherking specification lanaguage.
At the end of the run of the project you will get an html report of the steps run, html logs of activities while the test is running. You will also get screenshots of the scenarios as they run. 

## Test cases and dataset
The test cases are specified in the Gherkin specification file. Validating_user_registration_page.feature. Test cases are recorded in Examples or datatables.


## Resources
In order to successfully run this code base you will need the following. 
Have the latest Maven installed. [See installing maven](https://maven.apache.org/).
You will need Java 8. [See how to install Java.](https://java.com/en/download/help/download_options.html)
Cucumber, Gherkin specification - these will be installed once the pom.xml is run.
You should git installed. 

NBS! This was done in IntelliJ IDEA, which translate Gherkin slightly different from Eclipse with respect to converting variables. 
For example with this Gherkin specification. 

`Then I get the following "response"`

> IntelliJ IDEA will translate the stepDefinitions like below
>  
> ```
>     @Then("I get the following {string}")
>     public void i_get_the_following(String response) {
>          ...
>      }
> Eclipse will translate it like this
>
>     @Then("I get the following \[\d\]\")
>     public void i_get_the_following(String response) {
>         ...
>     }

## Setup
1. Navigate to your working directory
2. clone the repo with this commmand "git clone https://gitlab.com/open-pm/miro-cucumber-java.git"
3. Change directory to the local repository "`cd miro-cucumber-java"`
4. If you have install maven then run "`mvn clean`" to build project
5. Run "mvn test" to run the application. This will download the dependencies from maven central repo to your local instance. 
6. If you are running from idea you can right click on the "RunCucumberTest.java and select run as unittest to run the app.
7. Reports are located in the following directory
    logs/****.html
    screen_shots/
    target/cucumberreports.html
8. Detailed reporsts can also be found at my [cucumber reports repo here ](https://reports.cucumber.io/report-collections/665ed0a1-387f-4484-a376-58ceeed0079a)
